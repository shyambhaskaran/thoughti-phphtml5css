<html>
<head>
	<title>sign up</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script> 
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.5.4/datepicker.css" />
	<link rel="stylesheet"href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/themes/redmond/jquery-ui.css"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
	<style type="text/css">
    .bs-example{
    	margin: 20px;
    }
</style>
	<script type="text/javascript">
		function Validator()
		{
			var password = $("#password").val();
			var confirmPassword = $("#repassword").val();
			
			if (password != confirmPassword) 
			{
				event.preventDefault();
				alert("Passwords do not match.");
				return false;
			}
			var x = document.getElementById('loginForm');
			if ( ( x.gender[0].checked == false ) && ( x.gender[1].checked == false ) ) 
			{
				alert ( "Please choose your Gender: Male or Female" ); 
				return false;
			}
			
			var states= document.getElementById('state');
			if(states.value==" ")
			{
				alert("please select atleast one state");
				return false;
			}
			
		}
		function display_alert()
		{
			alert("login successfully");
		}
	
</script>
</head>
<body style="background-color:powderblue;">
<form id="loginForm" name="loginForm" method="POST">
<div align="center" style="vertical-align:bottom">
<div align="center" style="vertical-align:bottom">
<div class="container">
		<font size=6><b>Student Registration Form</b></font>
		<div class="form-group">
		<label for="usr">Name:</label>
		<input type="text" name="textnames" id="textname" class="form-control" required placeholder="first name" method="post">
		</div>
		<div class="form-group">
		<label for="usr">Email ID:</label>
		<input type="text" name="emailid" id="emailid" size="30" class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required placeholder="Enter you Email">
		</div>
		<div class="form-group">
		<label for="usr">password:</label>
		<input type="password" name="password" id="password"size="30" class="form-control" pattern=".{9,}" placeholder="enter your password">
		</div>
		<div class="form-group">
		<label for="usr">Re-type password:</label>
		<input type="password" name="repassword" id="repassword"size="30" class="form-control" placeholder="Re-type password">
		</div>
		<div class="form-group">
		<label for="usr">Phone:</label>
		<input type="text" name="phone" id="phone"size="30" pattern="[1-9]{1}[0-9]{9}" class="form-control" required placeholder="Enter phone no">
		</div>
	<div class="form-group">
	<label for="usr">Select a State</label>
	<select class="form-control" name="state" id="state">
		<option value="AP">Andra Pradesh</option>
		<option value="AR">Arunachal Pradesh</option>
		<option value="AS">Assam</option>
		<option value="BH">Bihar</option>
		<option value="CH">Chhattisgarh</option>
		<option value="MP">Madya Pradesh</option>
		<option value="MH">Maharashtra</option>
		<option value="IA">Iowa</option>
		<option value="KS">Kansas</option>
		<option value="KY">Kentucky</option>
		<option value="LA">Louisiana</option>
		<option value="ME">Maine</option>
		<option value="MD">Maryland</option>
		<option value="MA">Massachusetts</option>
		<option value="MI">Michigan</option>
		<option value="MN">Minnesota</option>
		<option value="MS">Mississippi</option>
		<option value="MO">Missouri</option>
		<option value="MT">Montana</option>
		<option value="NE">Nebraska</option>
		<option value="NV">Nevada</option>
		<option value="NH">New Hampshire</option>
		<option value="NJ">New Jersey</option>
		<option value="NM">New Mexico</option>
		<option value="NY">New York</option>
		<option value="NC">North Carolina</option>
		<option value="ND">North Dakota</option>
		<option value="OH">Ohio</option>
		<option value="OK">Oklahoma</option>
		<option value="OR">Oregon</option>
		<option value="PA">Pennsylvania</option>
		<option value="RI">Rhode Island</option>
		<option value="SC">South Carolina</option>
		<option value="SD">South Dakota</option>
		<option value="TN">Tennessee</option>
		<option value="TX">Texas</option>
		<option value="UT">Utah</option>
		<option value="VT">Vermont</option>
		<option value="VA">Virginia</option>
		<option value="WA">Washington</option>
		<option value="WV">West Virginia</option>
		<option value="WI">Wisconsin</option>
		<option value="WY">Wyoming</option>
	</select>
	</div>
	<div class="form-group">
			<label for="usr">Gender:</label>
			<label class="radio-inline">
			<input type="radio" id="gender" name="gender" class="radioBtnClass" value="1">Female
			</label>
			<label class="radio-inline">
			<input type="radio" id="gender" name="gender" value="2" class="radioBtnClass">Male
			</label>
	</div>
	<div class="form-group">
			<label for="usr">Language:</label>	
			<label class="checkbox-inline">
			<input type="checkbox" id="language[]" name="language[]" value="english">English
			</label>
			<label class="checkbox-inline">
			<input type="checkbox" id="language[]" name="language[]" value="hindi">Hindi
			</label>
			<label class="checkbox-inline">
			<input type="checkbox" id="language[]" name="language[]" value="marathi">Marathi
			</label>
	</div>
	<div class="form-group">
			<input type="checkbox" checked="checked"> Remember me<p>
	</div>
	<div class="form-group">
			By creating an account you agree to our <a href="#">Terms & Privacy</a>
	</div>
	<div class="form-group">
			<input type="submit" class="btn btn-primary" name="submit" id="submit" value="Submit" onclick="Validator();" class="signupbtn" size="50" style="height:35px;width:100px">
			</input>
			<input type="reset" class="btn btn-primary" value="Reset" size="50" style="height:35px;width:100px">
		</div>
	<div class="form-group">
		If you have Userid and password then <a href="login1.php" style="height:35px;width:100px" value="Login" role="button">Click here</a>
	</div>
</form> 
</div>
</body>
<?php
// include_once 'connection.php';
$conn = mysqli_connect("localhost","root","","information");
 if(isset($_POST["submit"]))
	{
	 $emailid=$_POST["emailid"];
	 $query= mysqli_query($conn,"SELECT emailid FROM stud WHERE emailid='".$_POST["emailid"]."'");
	  if (mysqli_fetch_assoc($query) != 0)
		{
		echo "<script> alert('email id exits'); 
		</script>";
		}
	else{
			$textnames=$_POST["textnames"];
			$password = md5($_POST['password']);
			$phone=$_POST["phone"];
			$state=$_POST["state"];
			$gender=$_POST["gender"];
			$checkbox1 = $_POST['language'];
			$chk=""; 
			foreach($checkbox1 as $chk1)
			{ 
					$chk.= $chk1.","; 
			} 
			$run = mysqli_query($conn, "INSERT INTO stud(textnames, emailid, password,phone, state, gender, language) VALUES('".$textnames. "','".$emailid."', '".$password."','".$phone."','".$state."','".$gender."','".$chk."')" );
			if (!$run){
			echo "<script> alert('not registerd'); 
			</script>";
			} else {
			echo "<script>
			alert('register successfully'); 
			</script>";	
			}
		}
  }
	
  
?>
